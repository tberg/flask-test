flask-test
==========

Setup:

	python3 -m venv venv
	. venv/bin/activate
	pip install -r requirements.txt


Update requirements:

	pip freeze > requirements.txt


Run:

	export FLASK_ENV=development
	flask run